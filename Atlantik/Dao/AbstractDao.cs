﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Atlantik.Tools;
using System.IO;
using System.Xml.Serialization;

namespace Atlantik.Dao
{
    abstract class AbstractDao<T> : IDao<T>
    {
        protected MySqlConnection Connection { get; private set; }

        public DatabaseConfiguration DatabaseConfiguration { get; set; }

        public AbstractDao()
        {
            DatabaseConfiguration = new DatabaseConfiguration();
            Connection = new MySqlConnection();
        }

        public AbstractDao(string path)
        {
            DatabaseConfiguration = DatabaseConfiguration.Load(path);
            Connection = new MySqlConnection(DatabaseConfiguration.ConnectionString);
        }

        public AbstractDao(DatabaseConfiguration dbConfig)
        {
            DatabaseConfiguration = dbConfig;
            Connection = new MySqlConnection(DatabaseConfiguration.ConnectionString);
        }

        protected void Open()
        {
            try
            {
                Connection.Open();
            }
            catch(MySqlException e)
            {
                throw new Exception("Cannot open connection.", e);
            }
        }

        protected void Close()
        {
            try
            {
                Connection.Close();
            }
            catch (MySqlException e)
            {
                throw new Exception("Cannot close connection.", e);
            }
        }

        public abstract int Create(T item);
        public abstract int Delete(T item);
        public abstract T Read(int id);
        public abstract List<T> ReadAll();
        public abstract int Update(T item);
    }
}
