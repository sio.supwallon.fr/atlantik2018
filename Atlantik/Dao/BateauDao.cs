﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlantik.Classes;
using Atlantik.Tools;
using MySql.Data.MySqlClient;

namespace Atlantik.Dao
{
    class BateauDao : AbstractDao<Bateau>
    {
        public BateauDao() : base() { }

        public BateauDao(string path) : base(path) { }

        public BateauDao(DatabaseConfiguration dbConfig) : base(dbConfig) { }

        public int CreateTable()
        {
            Console.WriteLine("CREATE TABLE `bateau` (...);");

            int res;

            string cmdText = @"
                CREATE TABLE IF NOT EXISTS `bateau`
                (
                    `id`       INT NOT NULL AUTO_INCREMENT,
                    `nom`      VARCHAR(255) NOT NULL,
                    `longueur` FLOAT NOT NULL,
                    `largeur`  FLOAT NOT NULL,
                    PRIMARY KEY (`id`)
                )Engine=InnoDB;
            ";

            Open();
            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            res = cmd.ExecuteNonQuery();
            Close();

            return res;
        }

        public override int Create(Bateau item)
        {
            throw new NotImplementedException();
        }

        public override int Delete(Bateau item)
        {
            throw new NotImplementedException();
        }

        public override Bateau Read(int id)
        {
            string cmdText = @"
                SELECT *
                FROM `bateau`
                WHERE id = ?id;
            ";

            Open();

            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            cmd.Prepare();
            cmd.Parameters.Add(new MySqlParameter("id", id));
            MySqlDataReader reader = cmd.ExecuteReader();

            if(! reader.Read())
            {
                throw new ArgumentException("Unknown id");
            }

            int _id = reader.GetInt32("id");
            string _nom = reader.GetString("nom");
            decimal _longeur = reader.GetDecimal("longueur");
            decimal _largeur = reader.GetDecimal("largeur");

            Bateau bateau = new Bateau(_id, _nom, _longeur, _largeur);

            reader.Close();
            Close();

            return bateau;
        }

        public override List<Bateau> ReadAll()
        {
            string cmdText = @"
                SELECT *
                FROM `bateau`;
            ";

            Open();

            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            MySqlDataReader reader = cmd.ExecuteReader();

            List<Bateau> bateaux = new List<Bateau>();

            while (reader.Read())
            {
                int _id = reader.GetInt32("id");
                string _nom = reader.GetString("nom");
                decimal _longeur = reader.GetDecimal("longueur");
                decimal _largeur = reader.GetDecimal("largeur");

                Bateau bateau = new Bateau(_id, _nom, _longeur, _largeur);

                bateaux.Add(bateau);
            }

            reader.Close();
            Close();

            return bateaux;
        }

        public override int Update(Bateau item)
        {
            throw new NotImplementedException();
        }
    }
}
