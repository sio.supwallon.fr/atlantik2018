﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlantik.Classes;
using Atlantik.Tools;
using MySql.Data.MySqlClient;

namespace Atlantik.Dao
{
    class BateauVoyageurDao : AbstractDao<BateauVoyageur>
    {
        public BateauVoyageurDao() : base() { }

        public BateauVoyageurDao(string path) : base(path) { }

        public BateauVoyageurDao(DatabaseConfiguration dbConfig) : base(dbConfig) { }

        public int CreateTable()
        {
            Console.WriteLine("CREATE TABLE `bateau_voyageur` (...);");

            int res;

            string cmdText = @"
                CREATE TABLE IF NOT EXISTS `bateau_voyageur`
                (
                    `id_bateau` INT NOT NULL,
                    `vitesse`   FLOAT NOT NULL,
                    `image`     VARCHAR(255) NOT NULL,
                    PRIMARY KEY (`id_bateau`),
                    FOREIGN KEY (`id_bateau`) REFERENCES `bateau`(`id`)
                )Engine=InnoDB;
            ";

            Open();
            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            res = cmd.ExecuteNonQuery();
            Close();

            return res;
        }

        public override int Create(BateauVoyageur item)
        {
            throw new NotImplementedException();
        }

        public override int Delete(BateauVoyageur item)
        {
            throw new NotImplementedException();
        }

        public override BateauVoyageur Read(int id)
        {
            throw new NotImplementedException();
        }

        public override List<BateauVoyageur> ReadAll()
        {
            throw new NotImplementedException();
        }

        public override int Update(BateauVoyageur item)
        {
            throw new NotImplementedException();
        }
    }
}
