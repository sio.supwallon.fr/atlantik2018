﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlantik.Classes
{
    class Equipement
    {
        // Propriété(s)

        public int Id { get; set; }
        public string Libelle { get; set; }

        // Constructeur(s)

        public Equipement() { }

        public Equipement(int id, string libelle)
        {
            Id = id;
            Libelle = libelle;
        }

        // Méthode(s)

        /// <summary>
        /// Retourne sous la forme d'une chaîne la valeur de l'attribut libelle de l'équipement.
        /// L'identifiant de l'équipement n'est pas inséré dans la chaîne.
        /// </summary>
        public override string ToString()
        {
            return Libelle;
        }
    }
}
