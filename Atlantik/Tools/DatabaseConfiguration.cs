﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace Atlantik.Tools
{
    public class DatabaseConfiguration
    {
        public string Server { get; set; } = "127.0.0.1";

        public string Port { get; set; } = "3306";

        public string Uid { get; set; } = "";

        public string Password { get; set; } = "";

        public string Database { get; set; } = "";

        public string Charset { get; set; } = "utf8";

        [XmlIgnore]
        public string ConnectionString
        {
            get
            {
                string str = "";
                str += "Server=" + Server + ";";
                str += "Port=" + Port + ";";
                str += "Uid=" + Uid + ";";
                str += "Password=" + Password + ";";
                str += "Database=" + Database + ";";
                str += "Charset=" + Charset + ";";
                return str;
            }
        }

        public static DatabaseConfiguration Load(string path)
        {
            DatabaseConfiguration dbConfig;
            using (FileStream stream = File.OpenRead(path))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(DatabaseConfiguration));
                dbConfig = (DatabaseConfiguration)xmlSerializer.Deserialize(stream);
            }

            return dbConfig;
        }

        public void Save(string path)
        {
            using (FileStream stream = File.Create(path))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(DatabaseConfiguration));
                xmlSerializer.Serialize(stream, this);
            }
        }
    }
}
