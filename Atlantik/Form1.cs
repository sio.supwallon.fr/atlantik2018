﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Atlantik.Classes;
using Atlantik.Dao;
using Atlantik.Tools;

namespace Atlantik
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCreateDatabase_Click(object sender, EventArgs e)
        {
            DatabaseConfiguration dbConfig;
            string path = Application.StartupPath + @"\dbConfig.xml";

            // First Config to save
            /*
            dbConfig = new DatabaseConfiguration();
            dbConfig.Database = "atlantik";
            dbConfig.Uid = "atlantik";
            dbConfig.Password = "Q8Cl00NXPecVXNv3";

            dbConfig.Save(path);
            */

            // Load config
            dbConfig = DatabaseConfiguration.Load(path);

            // CREATE TABLE bateau
            BateauDao bateauDao = new BateauDao(dbConfig);
            bateauDao.CreateTable();

            // CREATE TABLE bateau_fret
            BateauFretDao fretDao = new BateauFretDao(dbConfig);
            fretDao.CreateTable();

            // CREATE TABLE bateau_voyageur
            BateauVoyageurDao voyageurDao = new BateauVoyageurDao(dbConfig);
            voyageurDao.CreateTable();

            // CREATE TABLE equipement
            EquipementDao equipementDao = new EquipementDao(dbConfig);
            equipementDao.CreateTable();
            // CREATE TABLE bateau_equipement
            equipementDao.CreateLinkTable();
        }

        private void btnBrochurePDF_Click(object sender, EventArgs e)
        {
            // todo
            throw new NotImplementedException();
        }
    }
}
